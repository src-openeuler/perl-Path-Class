%define         perl_bootstrap 1

Name:		    perl-Path-Class
Version:	    0.37
Release:	    15
Summary:	    Cross-platform path specification manipulation
License:	    GPL-1.0-or-later OR Artistic-1.0-Perl
URL:		    https://metacpan.org/release/Path-Class

Source0:	    https://cpan.metacpan.org/authors/id/K/KW/KWILLIAMS/Path-Class-%{version}.tar.gz

BuildArch:	    noarch
BuildRequires:	coreutils make perl-interpreter perl-generators perl(Module::Build) > 0.36 perl(Carp)
BuildRequires:	perl(Cwd) perl(Exporter) perl(File::Copy) perl(File::Path) perl(File::Spec) >= 3.26
BuildRequires:	perl(File::stat) perl(File::Temp) perl(IO::Dir) perl(IO::File) perl(overload)
BuildRequires:	perl(parent) perl(Perl::OSType) perl(Scalar::Util) perl(strict) perl(Test)
BuildRequires:	perl(Test::More) >= 0.88 perl(warnings)
%if !%{defined perl_bootstrap}
BuildRequires:	perl(English) perl(Test::Perl::Critic)
%endif
Requires:	    perl(File::Copy) perl(Perl::OSType)

%description
Path::Class is a module for manipulation of file and directory specifications (strings describing 
their locations, like '/home/ken/foo.txt' or 'C:\Windows\Foo.txt') in a cross-platform manner. It 
supports pretty much every platform Perl runs on, including Unix, Windows, Mac, VMS, Epoc, Cygwin, 
OS/2, and NetWare.

%package_help

%prep
%autosetup -n Path-Class-%{version}

%build
perl Build.PL --installdirs=vendor
./Build

%install
./Build install --destdir=%{buildroot} --create_packlist=0
%{_fixperms} %{buildroot}

%check
AUTHOR_TESTING=1 ./Build test

%files
%doc README
%license LICENSE
%{perl_vendorlib}/Path/

%files help
%doc Changes
%{_mandir}/man3/Path::Class.3*
%{_mandir}/man3/Path::Class::Dir.3*
%{_mandir}/man3/Path::Class::Entity.3*
%{_mandir}/man3/Path::Class::File.3*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 0.37-15
- drop useless perl(:MODULE_COMPAT) requirement

* Fri Jan 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.37-14
- Package init
